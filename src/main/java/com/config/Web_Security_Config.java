package com.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;




@Configuration
public class Web_Security_Config implements WebMvcConfigurer  {
	

	
		@Override
	    public void addViewControllers(ViewControllerRegistry registry) {
	        //this will map uri to jsp view directly without a controller
	        registry.addViewController("/login")
	                .setViewName("login");
		}

	

}
