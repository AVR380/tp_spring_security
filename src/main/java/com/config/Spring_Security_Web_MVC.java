package com.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.service.User_Service;




@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
		prePostEnabled = true,
		securedEnabled = true,
		jsr250Enabled = true)

public class Spring_Security_Web_MVC extends WebSecurityConfigurerAdapter {
	
	
	@Autowired
	User_Service user_details_service;

	

	
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		
//		http
//			.authorizeRequests()
//			.anyRequest()
//			.fullyAuthenticated()
//			.and()
//			.httpBasic();
//		
//		http.csrf().disable();
		
		
		
		http.authorizeRequests()
			.antMatchers("/", "/index", "/login", "/security/login",
					"/security/logout", "/users/create")
			.permitAll()
			.and()
    		.authorizeRequests()
			.anyRequest()
			.authenticated()
			.and()
		.formLogin()
			.failureForwardUrl("/failure")
			.loginPage("/login")
			.permitAll()
			.and()
		.logout()
			.logoutSuccessUrl("/")
	    	.invalidateHttpSession(true)
	    	.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
			.and()
		.authorizeRequests()
			.antMatchers("/user/**")
			.hasRole("ADMIN")
			.and()
		.authorizeRequests()
			.antMatchers("user/message")
			.hasRole("USER");
			
	
		
		
		
	}
	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication()
//		.withUser("dev").password("{noop}dev").roles("dev")
//		.and()
//		.withUser("admin").password("{noop}admin").roles("admin");
		auth.userDetailsService(user_details_service);
	
	}
	
	
	
	
	

}
