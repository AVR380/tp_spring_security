package com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.model.User;
import com.repo.User_Repository;



@SpringBootApplication
public class SringSecurityApplication implements CommandLineRunner{

	
	@Autowired
	private User_Repository user_repository;
	
	public static void main(String[] args) {
		SpringApplication.run(SringSecurityApplication.class, args);
	}

	
	
	
	@Override
	public void run(String... args) throws Exception {
		User user = new User();
		user.setName("ADMIN");
		user.setRole("ADMIN");
		user.setPassword("ADMIN");
		
		User user2 = new User();
		user2.setName("gav");
		user2.setRole("USER");
		user2.setPassword("gav");
		
		User user3 = new User();
		user3.setName("test");
		user3.setRole("SCRUM_MASTER");
		user3.setPassword("test");

		user_repository.save(user);
		user_repository.save(user2);
		user_repository.save(user3);
	}
	
	
	
	
	
	
	
}
