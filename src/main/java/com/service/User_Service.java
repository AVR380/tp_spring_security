package com.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.User;
import com.repo.User_Repository;




@Service
public class User_Service implements UserDetailsService {
	
	private final User_Repository user_repository;
	
	@Autowired
	public User_Service(User_Repository user_repository) {
		this.user_repository = user_repository;
	}

	
	
	@Override
	public UserDetails loadUserByUsername(String user_name) throws UsernameNotFoundException {
		
		Objects.requireNonNull(user_name);
		
		BCryptPasswordEncoder encoder = password_encoder();
		
		User user = user_repository.findUserWithName(user_name)
				.orElseThrow(() -> new UsernameNotFoundException("User not found"));
		
		return new org.springframework.security.core.userdetails.User(user.getName(),encoder.encode(user.getPassword()),getGrantedAuthorities(user));
	}
	
	
//	private Collection<GrantedAuthority> getGrantedAuthorities(User user) {
//		
//		Collection<GrantedAuthority> granted_athority = new ArrayList<>();
//		granted_athority.add(new SimpleGrantedAuthority("ROLE_USER"));
//		
//		return granted_athority;
//		
//	}
//	
	
	
	private Collection<GrantedAuthority> getGrantedAuthorities(User user){       
	
		Collection<GrantedAuthority> grantedAuthority = new ArrayList<>();

	    System.out.println( "Le role ==> " + user.getRole());
	
	    //TODO: Gérer ce cas .
	
	    if(user.getRole() != null)
	
	         if(user.getRole().equals("ADMIN")) {
	
	             grantedAuthority.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
	
	         }
	
	    grantedAuthority.add(new SimpleGrantedAuthority("ROLE_USER"));        
	    return grantedAuthority;

	}
	
	
	
	
	
	
	@Bean
	public BCryptPasswordEncoder password_encoder() {
		return new BCryptPasswordEncoder();
	}
	
	
	
	
}
