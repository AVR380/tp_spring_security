package com.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/user")
public class Spring_Controller {
	
	@GetMapping
    @Secured({"ROLE_ADMIN"})

	public ResponseEntity<User> getUser(){
		
		  //User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	       User u = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	       return new ResponseEntity<User>(u, HttpStatus.OK);
	
	
	}
	
	
	

	
	

}
