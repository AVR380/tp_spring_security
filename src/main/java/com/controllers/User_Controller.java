package com.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.service.User_Service;



@Controller
public class User_Controller {
	
	@Autowired
	User_Service user_Service;
	

	
    @RequestMapping({ "/", "/index" })
    public String index(Model model) {
 
        model.addAttribute("message", "Bonjour AJC");
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!(auth instanceof AnonymousAuthenticationToken)) {
        	
        	model.addAttribute("loged", true);
        	
        }
        

        return "index";
    }
    
 

	@RequestMapping(value = { "/employee" }, method = RequestMethod.GET)
	public String emp(Model model) {
		model.addAttribute("message", "Bonjour AJC");
		model.addAttribute("loged", true);
		
		return "employee";
	}

	
	@RequestMapping(value = { "/failure" }, method = RequestMethod.GET)
	public String log_failure(Model model) {
		model.addAttribute("message", "Bonjour AJC");
		
		return "failure";
	}
	
	
	

    


	
}
