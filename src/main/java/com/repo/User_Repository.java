package com.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.model.User;




@Repository
public interface User_Repository extends JpaRepository<User, Integer> {
	@Query("select u from User u where u.name = ?1")
	Optional<User> findUserWithName(String user_name);

}
